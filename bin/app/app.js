const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const tokopediaController = require('../controllers/tokopedia/tokopedia.controller');
const shopeeController = require('../controllers/shopee/shopee.controller');

function AppServer() {
    this.app = express();

    //plugin
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
        extended: true
    }))
    this.app.use(bodyParser.text());

    //routes
    this.app.get('/', (req, res) => {
        res.send('This server is running properly');
    });

    this.app.route('/tokopedia/v1/scrape')
        .get(tokopediaController.scrapingProduct) //documentation https://expressjs.com/en/guide/routing.html
    this.app.route('/shopee/v1/scrape')
        .get(shopeeController.scrapingProduct)
}

module.exports = AppServer;