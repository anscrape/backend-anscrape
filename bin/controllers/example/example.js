const wrapper = require('../../lib/utils/wrapper');

const example = (req, res) => {
    const response = {
        example: 'ini example'
    }

    wrapper.response(res, 'success', response, 'success get  all example', 200);
}

const example2 = (req, res) => {
    const response = {
        example: 'ini example'
    }

    wrapper.response(res, 'success', response, 'success get  all example', 200);
}


module.exports = {
    example,
    example2
}