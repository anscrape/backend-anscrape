const shopeeRequest = require('../../lib/shopee/shopeeRequest');
const wrapper = require('../../lib/utils/wrapper');
const delay = require('delay');
const cheerio = require('cheerio');


const scrapingProduct = async (req,res) => {
    const {store, page, size} = req.query;
    if (!store) {
        res.send({
            status: 'failed',
            data: '',
            message: `Shopee scrapper`,
            code: 200
        });
    }

    const cookie = await shopeeRequest.getCookie(store);
    let newCookie = [];
    cookie.map(cook => {
        newCookie.push(cook.split(';')[0])
    })

    const shopId = await shopeeRequest.getIdByUsername(newCookie, store)
    if (shopId.length === 0) {
        res.send({
            status: 'failed',
            data: '',
            message: `Can't found shopname ${store}`,
            code: 404
        });
    }
    
    const idShop = shopId[0][store];
    const itemList = await shopeeRequest.getListItemsId(idShop, size);
    const newProd = [];
    for (let index = 0; index < itemList.items.length; index++) {
        const items = itemList.items[index];
        await delay(5000);
        try{
            const detailProduct = await shopeeRequest.getDetailProduct(items.itemid, idShop);
            newProd.push(detailProduct)
        }catch(e){
            res.send({
                status: 'failed',
                data: '',
                message: `failed scrapping page, internal server error`,
                code: 500
            });
        }
        
    }

    res.send({
        status: 'success',
        data: newProd,
        message: `success scrapping store ${store}`,
        code: 200
    });
}


module.exports = {
    scrapingProduct
}