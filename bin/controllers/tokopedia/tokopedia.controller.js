const tokopediaRequest = require('../../lib/tokopedia/tokopediaRequest');
const wrapper = require('../../lib/utils/wrapper');
const delay = require('delay');
const cheerio = require('cheerio');


const scrapingProduct = async (req,res) => {
    const {store, page, size} = req.query;
    if (!store || !size) {
        res.send({
            status: 'failed',
            data: '',
            message: `Tokopedia scrapper`,
            code: 200
        });
    }

    const resultHeaders = await tokopediaRequest.getHeaders();
    const buildCookie = resultHeaders.join();
    const resultData = await tokopediaRequest.getData(buildCookie, store, page, size)
    const $ = cheerio.load(resultData);
    let allProductUrl = [];
    const getBody = await $('a[class=css-aobwgn]').each(function(i, element) {
        const b = $(this).attr('href');
        allProductUrl.push(b);
    }); 
    const newProd = [];  
    for (let index = 0; index < allProductUrl.length; index++) {
        const element = allProductUrl[index];
        await delay(5000);
        try{
            const detailProduct = await tokopediaRequest.getProductDetail(buildCookie, element);
            newProd.push(detailProduct);
        }catch(e){
            res.send({
                status: 'failed',
                data: '',
                message: `failed scrapping page, internal server error`,
                code: 500
            });
        }
        
    }
    
    res.send({
        status: 'success',
        data: newProd,
        message: `success scrapping store ${store}`,
        code: 200
    });
}


module.exports = {
    scrapingProduct
}