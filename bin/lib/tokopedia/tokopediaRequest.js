const fetch = require('node-fetch');
const cheerio = require('cheerio');


const getHeaders = () => new Promise((resolve, reject) => {
    fetch('https://www.tokopedia.com/', {
        method:'GET',
        headers: {
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
        }
    })
    .then(res => {
        resolve(res.headers.raw()['set-cookie'])
    })
    .catch(err => {
        reject(err)
    })
});

const getData = (cookie, store, page,size) => new Promise((resolve, reject) => {
    fetch(`https://www.tokopedia.com/${!page ? store : `/page/${page}`}?perpage=${size}`, {
        method:'GET',
        headers: {
            "cookie": cookie,
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
        }
    })
    .then(res => res.text())
    .then(result => resolve(result))
    .catch(err => {
        reject(err)
    })
});


const getProductDetail = (cookie, urlProduct) => new Promise((resolve, reject) => {
    fetch(urlProduct, {
        method:'GET',
        headers: {
            "cookie": cookie,
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
        }
    })
    .then(res => res.text())
    .then(async result => {
        const $ = await cheerio.load(result);
        const productImg = $('div.content-img.content-main-img.product-detail__fixed-container img').attr('src');
        const productName = $('span[itemprop=name]').text();
        const productPrice = $('span[itemprop=price]').text();
        const description = $('div[itemprop=description]').text().replace(/\s+/g," ");
        const qty = $('input[name=quantity]').attr('data-stock-qty');
        const minBuy = $('span[class=min-num-text]').text().replace(/\s+/g,"");
        const maxBuy = $('span[class=max-num-text]').text().replace(/\s+/g,"");
        const condition = $('div.rvm-product-info--item_value.mt-5').text().replace(/\s+/g,"").split('1')[0];
        const res = {
            productImg,
            productName,
            productPrice,
            description,
            qty,
            minBuy,
            maxBuy,
            condition
        }
        resolve(res);
        
    })
    .catch(err => {
        reject(err)
    })
});

module.exports = {
    getHeaders,
    getData,
    getProductDetail
}
