const fetch = require('node-fetch');

const getDetailProduct = (itemId, shopId) => new Promise((resolve, reject) => {
    fetch(`https://shopee.co.id/api/v2/item/get?itemid=${itemId}&shopid=${shopId}`, {
        method: 'GET'
    })
    .then(res => res.json())
    .then(result => {
        const name = result.item.name;
        const image = `https://cf.shopee.co.id/file/${result.item.image}`
        const price = result.item.price.toString().split('0')[0]+'000';
        const description = result.item.description;
        const stock = result.item.stock;
        const sold = result.item.sold;
        const location = result.item.shop_location;
        const res = {
            name,
            image,
            price,
            description,
            stock,
            sold,
            location
        }
        resolve(res)
    })
    .catch(err => {
        reject(err)
    })
});

const getCookie = (username) => new Promise((resolve, reject) => {
    fetch(`https://shopee.co.id/api/v0/is_short_url/?path=${username}`, {
        method:'GET'
    })
    .then(res => resolve(res.headers.raw()['set-cookie']))
    .catch(err => {
        reject(err)
    })
});

const getIdByUsername = (cookie, username) => new Promise((resolve, reject) => {
    const boday = {"usernames":[`${username}`]}
    fetch('https://shopee.co.id/api/v1/shop_ids_by_username/', {
        method: 'POST',
        headers: {
            'sec-fetch-mode': 'cors',
            'origin': 'https://shopee.co.id',
            'accept-encoding': 'gzip, deflate, br',
            'accept-language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
            'x-requested-with': 'XMLHttpRequest',
            'cookie': `csrftoken=rUIy6523iQHdmF2VfwAVeQm2pKgD7zRV; ${cookie.join(';')}`,
            'if-none-match-': '55b03-1ae7d4aa7c47753a96c0ade3a9ea8b35',
            'x-csrftoken': 'rUIy6523iQHdmF2VfwAVeQm2pKgD7zRV',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
            'content-type': 'application/json',
            'accept': 'application/json',
            'referer': `https://shopee.co.id/${username}`,
            'authority': 'shopee.co.id',
            'sec-fetch-site': 'same-origin',
            'x-api-source': 'pc'
        },
        body: JSON.stringify(boday)
    })
    .then(res => res.json())
    .then(result => {
        resolve(result)
    })
    .catch(err => {
        reject(err)
    })
});

const getListItemsId = (shopId, size) => new Promise((resolve, reject) => {
    fetch(`https://shopee.co.id/search/api/items/?page_type=shop&match_id=${shopId}&keyword=&shop_categoryids=&hashtag=&facet_type=shop_facet&original_categoryid=&is_official_shop=&by=&order=asc&limit=${size}&need_drop_word=false&newest=0`,{
        method: 'GET'
    })
    .then(res => res.json())
    .then(result => {
        resolve(result)
    })
    .catch(err => {
        reject(err)
    })
});




module.exports = {
    getCookie,
    getDetailProduct,
    getIdByUsername,
    getListItemsId
}